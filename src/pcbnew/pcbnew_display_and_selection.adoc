:experimental:

== Display and selection controls

=== Board layers

Layers in Pcbnew represent physical copper layers on a board, as well as graphical layers used for
defining things such as silkscreen, solder mask, and the board edge.  There is always one layer
that is active in the editor.  The active layer is drawn on top of other layers and will be the
layer assigned to newly-created objects.  The active layer is indicated in the layer selector
drop-down box in the top toolbar and is also highlighted in the appearance panel.  To change the
active layer, you can left-click a layer name in the appearance panel, use the drop-down layer
selector in the top toolbar, or use a hotkey.  Layers can be hidden to simplify the board view.
You can hide a layer even if it is the active layer.

==== Display order for board layers

NOTE: TODO: Explain this.

=== The appearance panel

The appearance panel provides controls to manage the visibility, color, and opacity of objects in
the Pcbnew drawing canvas.  It has three tabs: the Layers tab contains controls for the board
layers, the Objects tab contains controls for different types of graphical objects, and the Nets
tab contains controls for the appearance of the ratsnest and copper items.

==== Layer controls

In the Layers tab of the appearance panel, each board layer is shown with its color and visibility
state.  The active layer is shown highlighted with an arrow indicator to the left of the color
swatch.  Left-click on a layer to choose it as the active layer.  Left-click on the corresponding
visibility icon to toggle the layer between visible and hidden.  Double-click or middle-click on
the color swatch to change the layer's color.

NOTE: You must first create a custom color theme in Preferences before you can change layer colors
      in the appearance panel.

Below the list of layers is an expandable panel that contains layer display options.  The first
setting controls how non-active layers are displayed: normal, dimmed, or hidden.  The layer display
mode can be used to simplify the view and focus on a single layer.  Items on inactive layers cannot
be selected when the non-active layer display mode is "Dim" or "Hide".  You can use the hotkey
kbd:[Ctrl+H] to cycle through these display modes quickly.

*Flip board view* will show the board as if you are looking from the bottom (that is, mirrored
around the Y-axis).  This option is also available in the View menu.

NOTE: Flipping the board view does not change the visual layer ordering, the active layer will
remain in front followed by the other layers in their normal order.

==== Object controls

The Objects tab of the appearance panel is similar to the Layers tab.  The main differences are
that some objects have no color setting and that four types of objects (tracks, vias, pads, and
zones) have opacity control sliders.  The opacity setting here will be multiplied with any opacity
set in the layer colors.  By default, all objects are fully opaque except for zones, which are set
to translucent in order to make it easier to see objects through filled zone areas.

==== Layer presets

Layer presets store which layers and objects are visible and hidden for easy recall.  There are
several built-in layer presets and you can save your own custom presets.  Custom presets are
stored in the project settings for a board, as presets may be specific to a certain board stackup.

To load a preset, choose it from the Presets drop-down menu at the bottom of the appearance panel
or use the quick switcher by holding down kbd:[Ctrl] and pressing kbd:[Tab].  Once the quick
switcher window appears, you can press kbd:[Tab] and kbd:[Shift+Tab] to cycle through the available
presets.  When you let go of the kbd:[Ctrl] key, the highlighted preset will be loaded.

To save a custom preset, first use the visibility controls to choose which layers you want visible,
then choose Save Preset... from the Presets drop-down menu.  Give your preset a name and it will
now be available via the drop-down menu and the quick switcher.  To modify a custom preset, follow
the same process and save the modified version with the same name to overwrite the existing
version.  To delete a custom preset, choose the Delete Preset... option from the drop-down menu and
select the preset to be deleted from the list.

==== Net and net class controls

The Nets tab of the appearance panel shows a list of all nets and net classes in the board.  Each
net has a visibility control that controls the visibility of that net in the ratsnest.  Hiding nets
in the ratsnest does not change the connectivity of the board and will not impact the design rule
checker; it only is intended to make the ratsnest easier to understand.

Each net and net class can also have a color assigned.  By default, this color applies to the
ratsnest lines for the net (or for all the nets in the net class).  Nets have no color by default;
this is indicated by a checkerboard pattern in the color swatch.  Double-click or right-click a
net or net class color swatch to set the color.

NOTE: The Default net class cannot have a color assigned, as nets in this class will just use the
      default ratsnest color defined by the color theme.

You can also select and highlight nets and net classes via the appearance panel: right-click on a
net or net class to show these options in a menu.

Below the list of net classes is an expandable panel that contains net display options.  The first
option controls how net colors are applied.  When "All" is selected, all copper items (pads,
tracks, vias, and zones) belonging to a net or net class will take on the chosen color.  When
"Ratsnest" is selected (the default value), only the ratsnest is affected by net and net class
colors.  When "None" is selected, net and net class colors are ignored.

The second option controls how ratsnest lines are drawn.  "All layers" means that ratsnest lines
will be drawn between all unconnected items.  "Visible layers" means that no ratsnest lines will
be drawn to items that are on hidden layers, even when those items are unconnected.

=== Selection and the selection filter

Selecting items in the editing canvas is done with the left mouse button.  Single-clicking on an
object will select it and dragging will perform a box selection.  A box selection from left to
right will only select items that are fully inside the box.  A box selection from right to left
will select any items that touch the box.  A left-to-right selection box is drawn in yellow and a
right-to-left selection box is drawn in blue.

The selection action can be modified by holding modifier keys while clicking or dragging.  The
following modifier keys apply when clicking to select single items:

[options="header",cols="20%,20%,60%",]
|====
| Modifier Keys (Windows/Linux) | Modifier Keys (macOS) | Selection Effect
| kbd:[Shift]       | kbd:[Shift]        | Add the item to the existing selection.
| kbd:[Alt+ Shift]  | kbd:[Cmd + Shift]  | Remove the item from the existing selection.
| kbd:[Alt]         | kbd:[Cmd]          | Toggle the selected state of the item.
| kbd:[Ctrl]        | kbd:[Alt]          | Clarify selection from a pop-up menu.
| kbd:[Ctrl+ Shift] | kbd:[Cmd + Option] | Highlight the net of the selected copper item.
|====

The following modifier keys apply when dragging to perform a box selection:

[options="header",cols="20%,20%,60%",]
|====
| Modifier Keys (Windows/Linux) | Modifier Keys (macOS) | Selection Effect
| kbd:[Shift]       | kbd:[Shift]        | Add item(s) to the existing selection.
| kbd:[Alt+ Shift]  | kbd:[Cmd + Shift]  | Remove item(s) to the existing selection.
| kbd:[Alt]         | kbd:[Cmd]          | Toggle the selected state of item(s).
|====

The selection filter panel in the lower right corner of the Pcbnew window controls which types of
objects can be selected with the mouse.  Turning off selection of unwanted object types makes it
easier to select items in a dense board.  The "All items" checkbox is a shortcut to turn the other
items on and off.  The "Locked items" checkbox is independent of the rest, and controls whether or
not items that have been locked can be selected.  You can right-click any object type in the
selection filter to quickly change the filter to only allow selecting that type of object.

When a connected copper item is selected, you can expand the selection to other copper items of the
same net using the Expand Selection command in the right-click context menu or with the hotkey
kbd:[U].  The first time you run this command, the selection will be expanded to the nearest pad.
The second time, the selection will be expanded to all connected items on all layers.

Pressing kbd:[Esc] will always cancel the current tool or operation and return to the selection
tool.  Pressing kbd:[Esc] while the selection tool is active will clear the current selection.

=== Net highlighting

An electrical net (or set of nets) can be highlighted in the PCB editor to visualize how the net
is routed across the PCB.  Net highlighting can be activated by selecting the net to highlight in
the PCB editor or by selecting the corresponding net in the schematic editor when cross-probe
highlighting is enabled (see below).  When net highlighting is active, the highlighted net or nets
will be shown in a brighter color and all other items will be shown in a dimmer color than normal.

There are three ways to select a net or nets to highlight in the PCB editor: by using the hotkey
kbd:[`] after selecting a copper object, by using the context menu of any copper object, and the
context menu of the Nets tab of the Appearance panel.  When you press the Highlight Net hotkey, the
nets of any selected copper items will be highlighted.  If no copper items are selected, the net of
the copper item under the editor cursor will be highlighted.  If there is no copper item under the
cursor, any existing highlighting will be cleared.  The highlighting can also be cleared by using
the Clear Highlight action (hotkey kbd:[~]).

When a net or nets have been selected for highlighting, the Toggle Net Highlighting action becomes
enabled on the left toolbar (also accessible by hotkey, kbd:[Ctrl+`]).  This action will
turn the highlighting display on or off without choosing a new net to highlight.

=== Cross-probing from the schematic

KiCad allows bi-directional cross-probing between the schematic and the PCB.  There are several
different types of cross-probing.

*Selection cross-probing* allows you to select a symbol or pin in the schematic to select the
corresponding footprint or pad in the PCB (if one exists) and vice-versa.  By default, cross-probing
will result in the display centering on the cross-probed item and zooming to fit.  This behavior
can be disabled in the Display Options section of the Preferences dialog.

*Highlight cross-probing* allows you to highlight a net in the schematic and PCB at the same time.
If the option "Highlight cross-probed nets" is enabled in the Display Options section of the
Preferences dialog, highlighting a net or bus in the schematic editor will cause the corresponding
net or nets to be highlighted in the PCB editor.

=== Left toolbar display controls

The left toolbar provides options to change the display of items in Pcbnew.

[width="100%",cols="5%,95%",]
|====
| image:images/icons/grid.png[]
    | Turns grid display on/off.

    *Note:* by default, hiding the grid will disable grid snapping.
    This behavior can be changed in the Display Options section of Preferences.
| image:images/icons/polar_coord.png[]
    | Switch between polar and Cartesian coordinate display in the status bar.
| image:images/icons/unit_inch.png[]

  image:images/icons/unit_mil.png[]

  image:images/icons/unit_mm.png[]
    | Display/entry of coordinates and dimensions in inches, mils, or millimeters.
| image:images/icons/cursor_shape.png[]
    | Switches between full-screen and small editing cursor (crosshairs).
| image:images/icons/general_ratsnest.png[]
    | Turns the ratsnest display on/off.
| image:images/icons/curved_ratsnest.png[]
    | Switches between straight and curved ratsnest lines.
| image:images/icons/contrast_mode.png[]
    | Switches the non-active layer display mode between Normal and Dim.

    *Note:* this button will
      be highlighted when the non-active layer display mode is either Dim or Hide.  In both cases,
      pressing the button will change the layer display mode to Normal.  The Hide mode can only be
      accessed via the controls in the Appearance Panel or via the hotkey kbd:[Ctrl+H].
| image:images/icons/net_highlight.png[]
    | When a net has been selected for highlighting, switches the highlighting on or off.

    *Note:* this button will be disabled when no net has been highlighted.  To highlight a net, use
    the hotkey kbd:[`], right-click any copper object in the net and choose Highlight Net from the
    Net Tools menu, or right-click the net in the list in the Nets tab of the Appearance panel.
| image:images/icons/show_zone.png[]
    | Show zone filled areas.
| image:images/icons/show_zone_disable.png[]
    | Show zone outlines only.
| image:images/icons/show_zone_outline_only.png[]
    | Show zone filled areas as outlines.
| image:images/icons/pad_sketch.png[]
    | Switches display of pads between filled and outline mode.
| image:images/icons/via_sketch.png[]
    | Switches display of vias between filled and outline mode.
| image:images/icons/showtrack.png[]
    | Switches display of tracks between filled and outline mode.
| image:images/icons/layers_manager.png[]
    | Shows or hides the Appearance and Selection Filter panels on the right side of the editor.

|====
